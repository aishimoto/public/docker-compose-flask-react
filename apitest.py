from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api, Resource, fields
import json


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy()
db.init_app(app)

class Test(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    value = db.Column(db.Text(), unique=False, nullable=False)


api = Api(app)

@api.route('/test/', endpoint='test-list')
class  TestList(Resource):
    def get(self):
        return [{'id':t.id, 'name':t.name, 'value':t.value} for t in Test.query.all()]

    def post(self):
        t = Test(**request.json)
        created = db.session.add(t)
        db.session.commit()
        return {'id':t.id, 'name':t.name, 'value':t.value}, 201



@api.route('/test/<int:id>', endpoint='test-resource')
class  TestResource(Resource):
    def get(self, id):
        t = Test.query.get(id)
        return {'id':t.id, 'name':t.name, 'value':t.value}

    def put(self, id):
        t = Test.query.get(id)
        for k, v in request.json.items():
            print(k, v)
            setattr(t, k, v)
        db.session.commit()
        return {'id':t.id, 'name':t.name, 'value':t.value}


with app.app_context():
    db.create_all()

