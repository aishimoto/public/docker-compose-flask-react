FROM python:3
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
RUN mkdir /env
WORKDIR /env
RUN python3 -m venv /env/venv
RUN /env/venv/bin/pip install poetry
COPY pyproject.toml /env/
COPY poetry.lock /env/
RUN . /env/venv/bin/activate && /env/venv/bin/poetry install
WORKDIR /code
