import React from "react";
import "./App.css";
import Axios from "axios";

type Test = {
  id: number;
  name: string;
  value: string;
};

class App extends React.Component<
  {},
  { name: string; value: string; entries: Test[] }
> {
  constructor(props: {}) {
    super(props);
    this.state = { name: "", value: "", entries: [] };
    const self = this;
    Axios.get("/test/").then(ret => {
      self.setState({ entries: ret.data });
    });
  }
  onSumbit = async (e: React.FormEvent) => {
    e.preventDefault();
    e.stopPropagation();

    await Axios.post("/test/", {
      name: this.state.name,
      value: this.state.value
    });
    this.setState({ name: "", value: "" });

    const self = this;
    Axios.get("/test/").then(ret => {
      self.setState({ entries: ret.data });
    });
    this.state = { name: "", value: "", entries: [] };
  };
  onNameChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({
      name: event.currentTarget.value
    });
  };

  onValueChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({
      value: event.currentTarget.value
    });
  };

  render = () => {
    return (
      <div>
        <form onSubmit={this.onSumbit}>
          <div>
            <input
              name="name"
              value={this.state.name}
              onChange={this.onNameChange}
              placeholder="name"
            />
          </div>
          <div>
            <input
              name="value"
              value={this.state.value}
              onChange={this.onValueChange}
              placeholder="value"
            />
          </div>
          <button type="submit">register</button>
        </form>
        <div>
          {this.state.entries.map(t => {
            return (
              <div key={t.id}>
                {t.name}:{t.value}
              </div>
            );
          })}
        </div>
      </div>
    );
  };
}

export default App;
