# Flask/React開発用 Docker compose 設定サンプル


PythonとNode.jsを使ったプロジェクトでの、開発用 Docker-compose設定のサンプルです。

PythonとNodeの処理系と、依存ファイルはDockerコンテナで管理し、ソースコードはホストのディレクトリから読みkんで実行します。どちらも、ソースコードが変更されると自動的に再読み込みが発生します。

## 使い方

次のコマンドでコンテナを起動します。

```
git clone https://gitlab.com/aishimoto/public/docker-compose-flask-react.git

cd docker-compose-flask-react

docker-compose up
```

プラウザで、

    http://localhost:3000

にアクセスすると、react.jsで作成した画面を表示します。この画面でデータを入力してregisterボタンを押すと、node.jsサーバを介してFlaskへリクエストが転送されます。

    http://localhost:5000

にアクセスすると、Flaskに直接アクセスできます。


## ソースの編集

Flaskのソースコードは、`apitest.py` です。実行中にこのファイルを編集して保存すると、自動的にFlaskプロセスが再起動します。

フロントエンドのソースは、`testclient` ディレクトリに保存しています。こちらも、`testclient/src` 以下のソースファイルを変更すると、自動的に再ロードされます。


---------------------

Copyright 2020 Atsuo Ishmoto

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
